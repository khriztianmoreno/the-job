import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './scenes/Home';
import Login from './scenes/Sign/Login';
import Register from './scenes/Sign/Register';
import Candidates from './scenes/Candidates';
import AddJob from './scenes/Jobs/Create';
import ApplyJob from './scenes/Jobs/Apply';
import DetailJob from './scenes/Jobs/Detail';
import BrowseJobs from './scenes/Jobs/';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route
        path="/"
        exact
        component={Home}
      />
      <Route
        path="/jobs"
        exact
        component={BrowseJobs}
      />
      <Route
        path="/jobs/create"
        exact
        component={AddJob}
      />
      <Route
        path="/jobs/detail/:id"
        exact
        component={DetailJob}
      />
      <Route
        path="/jobs/apply/:id"
        exact
        component={ApplyJob}
      />
      <Route
        path="/candidates"
        exact
        component={Candidates}
      />
      <Route
        path="/login"
        exact
        component={Login}
      />
      <Route
        path="/register"
        exact
        component={Register}
      />
    </Switch>
  </BrowserRouter>
);

export default App;
